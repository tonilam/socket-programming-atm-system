/*	Client Login

	Author: Toni
	Date: September 2016
*/

#include "Menu.h"

void printWelcome(LoginSession* session) {
	printf("Welcome to the ATM System\n\n");
	printf("You are currently logged in as %s %s\n", session->firstName, session->lastName);
	printf("Client Number - %d\n", session->id);
}

void printMainMenu() {
	printf("\n\nPlease enter a selection\n");
	printf("<1> Account Balance\n");
	printf("<2> Withdrawal\n");
	printf("<3> Deposit\n");
	printf("<4> Transfer\n");
	printf("<5> Transaction Listing\n");
	printf("<6> EXIT\n");
	printf("\nSelection option 1-6 ->");
}

char readMainMenuChoice() {
	char input[3];
	bool validInput = false;
	do {
		printMainMenu();
        scanf("%s", input);
        if (
        		(input[1] != 0) ||
           		(input[0] < '1') ||
           		(input[0] > '6')
           	) {
            printf("Invalid selection. Please select option from menu!\n\n\n");
        } else {
        	validInput = true;
        }
    } while (!validInput);

    return input[0];
}

void printBalanceMenu(BalanceBuffer* balanceDetail) {
	char counter = 0;
	printf("\nSelect Account Type\n");
	if (balanceDetail->hasSaving) {
		printf("%d. Saving Account\n", ++counter);
	}
	if (balanceDetail->hasLoan) {
		printf("%d. Loan Account\n", ++counter);
	}
	if (balanceDetail->hasCredit) {
		printf("%d. Credit Card\n", ++counter);
	}
	printf("\nEnter your selection (E/e to exit) - ");
}

char readBalanceMenuChoice(BalanceBuffer* balanceDetail) {
	char input[3];
	bool validInput = false;
	bool exitSignal = false;
	do {
		printBalanceMenu(balanceDetail);
        scanf("%s", input);
        if (
        		(	(input[1] != 0) ||
           			(input[0] < '1') ||
           			(input[0] > '3')
           		) && (
           			(input[0] != 'e') &&
           			(input[0] != 'E')
           		)
           	) {
            printf("Invalid selection. Please select option from menu!\n\n\n");
        } else  if (
        		(input[0] == 'e') ||
        		(input[0] == 'E')
        	) {
        	exitSignal = true;
        } else{
        	validInput = true;
        }
    } while ((!validInput) && (!exitSignal));

    return input[0];
}