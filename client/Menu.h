/*	Client Login
	Description of this object:
	**TODO**
*/
#ifndef MENU_H_
#define MENU_H_

#include <stdio.h>
#include <stdlib.h>

#include "Login.h"
#include "AccountBalance.h"

void printWelcome(LoginSession* session);
void printMainMenu(void);
char readMainMenuChoice(void);
void printBalanceMenu(BalanceBuffer* balanceDetail);
char readBalanceMenuChoice(BalanceBuffer* balanceDetail);

#endif