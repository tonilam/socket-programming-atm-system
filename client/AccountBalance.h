/*	Client Login
	Description of this object:
	**TODO**
*/
#ifndef ACCOUNTBALANCE_H_
#define ACCOUNTBALANCE_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "Config.h"
#include "Login.h"
#include "Socket.h"
#include "../library/RequestCode.h"
#include "../library/Parameter.h"

typedef struct balanceBuffer {
	short int combination;
	bool hasSaving;
	bool hasLoan;
	bool hasCredit;
	int accountSaving;
	int accountLoan;
	int accountCredit;
	float balanceSaving;
	float balanceLoan;
	float balanceCredit;
} BalanceBuffer;

							//	Bitwise combination of accounts
							//   1 = exist / 0 = not exist
						   	//	CR LO SA
#define ACC_SA 1			//   0  0  1
#define ACC_LO 2			//   0  1  0
#define ACC_CR 4			//   1  0  0
#define ACC_COMBI_SA_LO 3	//   0  1  1
#define ACC_COMBI_SA_CR 5	//   1  0  1
#define ACC_COMBI_LO_CR 6	//   1  1  0
#define ACC_COMBI_ALL 7		//   1  1  1



BalanceBuffer* Balance(int clientId);
void destroyBalance(BalanceBuffer* instance);
void printBalanceOnScreen(char* fname, char* lname, int accNo, double closingBal);
void showBalance(LoginSession* session, BalanceBuffer* balanceDetail, char option);

#endif