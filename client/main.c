#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include "Config.h"
#include "Socket.h"
#include "Login.h"
#include "Menu.h"
#include "AccountBalance.h"

LoginSession* session;
BalanceBuffer* balanceDetail;

void testing(void);

int main(int argc, char *argv[]) {
	// declare new variables
    char hostip[16];
    int portno;
    char user[MAX_USERNAME];
	char password[MAX_PASSWORD];
    char rootAction = 0, layer2Action = 0;

    // validate the input parameters
    if (argc < 3) {
        printf("Usage %s hostname port\n", argv[0]);
        printf("The hostname and port number must be specified.\n\n");
        exit(0);
    }

    strcpy(hostip, argv[1]);
    portno = atoi(argv[2]);

    if (Socket(hostip, portno)) {
        printLoginScreen();
        readUserName(user);
        readPassword(password);

        session = Login(user, password);

        if (session->id > 0) {
            system("clear");
            printWelcome(session);
            do {
                rootAction = readMainMenuChoice();
                if (rootAction == '1') {
                    if (Socket(hostip, portno)) {
                        balanceDetail = Balance(session->id);
                        layer2Action = readBalanceMenuChoice(balanceDetail);
                        if (
                                (layer2Action != 'E') &&
                                (layer2Action != 'e')
                            ) {
                            showBalance(session, balanceDetail, layer2Action);
                        }
                        destroyBalance(balanceDetail);
                    }
                }
            } while (rootAction != '6');
        } else {
            printLoginFail();
        }

        if (DEBUG) {
            testing();
        }

        destroyLogin(session);
    }

    return 0;
}

void testing(void){
    // TODO
}