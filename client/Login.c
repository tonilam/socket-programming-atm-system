/*	Client Login

	Author: Toni
	Date: September 2016
*/

#include "Login.h"

LoginSession* Login(char* username, char* password) {
	char queryString[256], response[256];
	LoginSession* session = malloc(sizeof(LoginSession));
	LinkedList* parameter;

	sprintf(queryString, "%d %s %s", REQUEST_VERIFY_LOGIN, username, password);
	requestSocket(queryString, response);

	// extract the response string to session structure
	parameter = Parameter(response);
	session->id = atoi(getParam(parameter, 0));
	strcpy(session->firstName, getParam(parameter, 1));
	strcpy(session->lastName, getParam(parameter, 2));

	// gracefully free memory of useless variable
	destroyParameter(parameter);

	return session;
}

void destroyLogin(LoginSession* instance) {
	free(instance);
}

void printLoginScreen(void) {
	system("clear");
	printf("=============================================\n\n\n");
	printf("Welcome to the Online ATM System\n\n\n");
	printf("=============================================\n\n\n");

	printf("You are required to logon with your registered Username and PIN\n\n");
}

void readUserName(char* name) {
	printf("Please enter your username-->");
	scanf("%s", name);
}

void readPassword(char* pswd) {
	printf("Please enter your password-->");
	scanf("%s", pswd);
}

void printLoginFail(void) {
	printf("You entered either an incorrect Username or PIN - ");
	printf("disconnecting\n");
}