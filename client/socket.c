/**
 *	This is the socket object to process communication with server.
 *
 *	Author: Toni Lam
 *	Date: October 2016
 */

#include "Socket.h"

int sockfd;

bool Socket(char hostip[], int portno) {
	struct sockaddr_in serv_addr;
	struct hostent *server;

	// opening a socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
    	perror("ERROR opening socket");
    	return false;
	} 

	server = gethostbyname(hostip);

	if (server == NULL) {
    	printf("ERROR, no such host\n");
		return false;
	}

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(portno);

	/* Now connect to the server */
	if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
		perror("ERROR connecting");
		return false;
	}

	return true;
}

bool requestSocket(char* query, char* result) {
	int numbytes;
	char buffer[256];

	printf("TEST: Calling server...\n");
	numbytes = write(sockfd, query, strlen(query));
	if (numbytes < 0) {
		perror("ERROR writing to socket");
		return false;
	}
   
	/* Now read server response */
	bzero(buffer,256);
	numbytes = read(sockfd, buffer, 255);
   
	if (numbytes < 0) {
		perror("ERROR reading from socket");
		return false;
	}

	strcpy(result, buffer);

	printf("TEST: Disconnected from server...\n");
	
	return true;
}