/* system configuration */
#ifndef CONFIG_H_
#define CONFIG_H_

#include <stdbool.h>

#define DEBUG true

#define DEFAULT_PORT 12345

#define MAX_USERNAME 20
#define MAX_PASSWORD 30
#define MAX_NAME 30

#endif