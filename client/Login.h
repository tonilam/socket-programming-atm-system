/*	Client Login
	Description of this object:
	**TODO**
*/
#ifndef LOGIN_H_
#define LOGIN_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "Config.h"
#include "Socket.h"
#include "../library/RequestCode.h"
#include "../library/Parameter.h"

typedef struct loginSession {
	int id;
	char firstName[MAX_NAME];
	char lastName[MAX_NAME];
} LoginSession;

LoginSession* Login(char* username, char* password);
void destroyLogin(LoginSession* instance);
void printLoginScreen(void);
void readUserName(char* name);
void readPassword(char* password);
void printLoginFail(void);

#endif