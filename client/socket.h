/**
 *	This is the socket object to process communication with server.
 *
 *	Author: Toni Lam
 *	Date: October 2016
 */
#ifndef ATMSOCKET_H_
#define ATMSOCKET_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <unistd.h>

#include <netdb.h>
#include <netinet/in.h>

#include <string.h>

bool Socket(char hostip[], int portno);
bool requestSocket(char* query, char* result);

#endif