/*	Client Login

	Author: Toni
	Date: September 2016
*/

#include "AccountBalance.h"

/**
 *	This object will make a socket request from the server and get the account number of each account type.
 *	If the client does not hold any type of a account, the account number is 0.
 *
 *	Pre-condition: An existing client id is provided
 *	Post-condition:	The account number of the three account types should be returned from the server;
 *					and to be stored in the returning BalanceBuffer data structure.
 */
BalanceBuffer* Balance(int clientId) {
	char queryString[256], response[256];
	BalanceBuffer* balance = malloc(sizeof(BalanceBuffer));
	LinkedList* parameter;

	sprintf(queryString, "%d %d", REQUEST_GET_ACCOUNTS, clientId);
	printf("Loading account blanace for %d\n", clientId);
	requestSocket(queryString, response);
	printf("Response:\n%s\n", response);

	// extract the response string to session structure
	parameter = Parameter(response);

	balance->hasSaving = ((balance->accountSaving = atoi(getParam(parameter, 0))) > 0);
	balance->hasLoan = ((balance->accountLoan = atoi(getParam(parameter, 1))) > 0);
	balance->hasCredit = ((balance->accountCredit = atoi(getParam(parameter, 2))) > 0);

	balance->balanceSaving = atof(getParam(parameter, 3));
	balance->balanceLoan = atof(getParam(parameter, 4));
	balance->balanceCredit = atof(getParam(parameter, 5));

	balance->combination = 0;
	if (balance->hasSaving) {
		balance->combination += ACC_SA;
	}
	if (balance->hasLoan) {
		balance->combination += ACC_LO;
	}
	if (balance->hasCredit) {
		balance->combination += ACC_CR;
	}

	// gracefully free memory of useless variable
	destroyParameter(parameter);

	return balance;
}

/**
 *	Deconstructor of this object
 *	
 *	Pre-condition: instance is created and as an input of this method.
 *	Post-condition: the memory of this instance is released.
 */
void destroyBalance(BalanceBuffer* instance) {
	free(instance);
}

void printBalanceOnScreen(char* fname, char* lname, int accNo, double closingBal) {
	printf("\n================================================================================\n\n");
	printf("Account Name - %s %s\n\n", fname, lname);
	printf("Current Balance for Account %d : $%.2f\n\n", accNo, closingBal);
	printf("================================================================================\n\n");

}

void showBalance(LoginSession* session, BalanceBuffer* balanceDetail, char option) {
	int targetAccount = 0;
	double accountBal = 0;

	if (	// Option 1 possible a/c combination
			(option == '1') &&
			(
				(balanceDetail->combination == ACC_SA) ||
				(balanceDetail->combination == ACC_COMBI_SA_LO) ||
				(balanceDetail->combination == ACC_COMBI_SA_CR) ||
				(balanceDetail->combination == ACC_COMBI_ALL)
			)
		) {
		targetAccount = balanceDetail->accountSaving;
		accountBal = balanceDetail->balanceSaving;
	} else if (
				(	// Option 1 possible a/c combination
					(option == '1') &&
					(
						(balanceDetail->combination == ACC_LO) ||
						(balanceDetail->combination == ACC_COMBI_LO_CR)
					)
				) ||
				(	// Option 2 possible a/c combination
					(option == '2') &&
					(
						(balanceDetail->combination == ACC_COMBI_SA_LO) ||
						(balanceDetail->combination == ACC_COMBI_ALL)
					)  
				)
		) {
		targetAccount = balanceDetail->accountLoan;
		accountBal = balanceDetail->balanceLoan;
	} else if (
				(	// Option 1 possible a/c combination
					(option == '1') &&
					(balanceDetail->combination == ACC_CR)
					
				) ||
				(	// Option 2 possible a/c combination
					(option == '2') &&
					(
						(balanceDetail->combination == ACC_COMBI_SA_CR) ||
						(balanceDetail->combination == ACC_COMBI_LO_CR)
					) 
				) ||
				(	// Option 3 possible a/c combination
					(option == '3') &&
					(balanceDetail->combination == ACC_COMBI_ALL)
				)
		) {
		targetAccount = balanceDetail->accountCredit;
		accountBal = balanceDetail->balanceCredit;
	} else {
		printf("No valid innformation.\n");
		return;
	}
	printBalanceOnScreen(session->firstName, session->lastName, targetAccount, accountBal);

	// use the variable at least once to avoid compiling warning.
	targetAccount = accountBal;
	accountBal = targetAccount;

}