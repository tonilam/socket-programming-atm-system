#ifndef PARAMETER_H_
#define PARAMETER_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>

 typedef struct Node {
   char* data;
   struct Node *next;
 } LinkedList;

/* Define constant for this class */


/* Define global variable for this class */

/* Method signatures */
LinkedList* Parameter(char* queryString);
void destroyParameter(LinkedList* instance);
char* getParam(LinkedList* instance, int index);

#endif