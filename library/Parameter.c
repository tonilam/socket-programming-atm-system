#include "Parameter.h"

/**
 * //TODO:Desc
 */

LinkedList* Parameter(char* queryString) {
	// split the query string in to a linked list

	const char delim[2] = " ";	// the delimeter character to seperate each parameter

	LinkedList* params = NULL;
	LinkedList* currentNode;
	char *token;

	printf("Parameter to process: %s\n", queryString);
	token = strtok(queryString, delim);
	while(token != NULL) {

		// create a new node and feeding data
		LinkedList *newNode = malloc(sizeof(LinkedList));
		newNode->data = token;	// store the data
		//printf("extracting parameter: %s\n", newNode->data);
		newNode->next = 0;   	// new item should point to null, means end of list

		// checking for empty list
		if (params == NULL) {
			// generate new head node
			params = newNode;    // replace the current empty node with new node
			currentNode = params;  // point the last node to the current node
			currentNode->next = 0;
		} else {
			// append current node
			currentNode->next = newNode; 		// append the last pointer and point to the new node
			currentNode = currentNode->next;	// point the last node to the appended node
			currentNode->next = 0;
      }

      // read next token
      token = strtok(NULL, delim);
	}

	return params;
}

void destroyParameter(LinkedList* instance) {
	free(instance);
}

char* getParam(LinkedList* instance, int index) {
	LinkedList* tempList = instance;
	for (int n = 0; n < index; ++n) {
		tempList = tempList->next;
	}

	return tempList->data;
}
