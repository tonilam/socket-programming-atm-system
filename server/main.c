#include <stdio.h>
#include <stdlib.h>
#include <signal.h>


#include "Config.h"
#include "Auth.h"
#include "Account.h"
#include "Client.h"
#include "Transaction.h"
#include "Socket.h"

TransactionRec transaction;

void testing(void);
void signal_callback_handler(int signum);

int main(int argc, char *argv[]) {
	int portno;

	// Use the signal handler to capture the [Ctrl+C] interupt.
	signal(SIGINT, signal_callback_handler);

 	if (DEBUG) {
 		testing();
 	}
 	
    printf("-- ATM SERVER --\n");
 	Transaction(&transaction);

 	if (argv[1] != NULL) {
	 	portno = atoi(argv[1]);
 	} else {
 		portno = DEFAULT_PORT;
 	}
 	Socket(portno);
    
    return 0;
}

void testing(void){
	testAuth();
	testAccount();
	testClient();
	testTransaction();
}

/**
 *	This method handles the [Ctrl+C] input signal from user and attemp to terminate the program gracefully.
 *	
 *	Pre-condition: User press the Ctrl + C keys from keyboard.
 *	Post-condition: Program terminated and exit message is printed on the console.
 */
void signal_callback_handler(int signum) {
	printf("System shutdown. Goodbye!\n");
	exit(signum);
}