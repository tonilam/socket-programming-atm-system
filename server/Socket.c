/**
 *	This is the socket object to process communication with client.
 *
 *	Author: Toni Lam
 *	Date: October 2016
 */

#include "Socket.h"

bool Socket(int portno) {
	/* Thread and thread attributes */
	pthread_t client_thread;
	pthread_attr_t attr;

	int sockfd, newsockfd;
	struct sockaddr_in serv_addr, cli_addr;
	socklen_t sin_size;
   
   /* First call to socket() function */
   sockfd = socket(AF_INET, SOCK_STREAM, 0);
   
   if (sockfd < 0) {
      perror("ERROR opening socket");
      exit(1);
   }
   
   /* Initialize socket structure */
   bzero((char *) &serv_addr, sizeof(serv_addr));
   
   serv_addr.sin_family = AF_INET;           /* host byte order */
   serv_addr.sin_addr.s_addr = INADDR_ANY;   /* auto-fill with my IP */
   serv_addr.sin_port = htons(portno);       /* short, network byte order */
   
   /* Now bind the host address using bind() call.*/
   if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR on binding");
      exit(1);
   }

   /* listen to socket */
   if (listen(sockfd, BACKLOG) == -1) {
		perror("ERROR on listening");
		exit(1);
	}

	printf("Server started.\nListening incoming call...\n");
      
	/* Now start listening for the clients, here process will
	 * go in sleep mode and will wait for the incoming connection
	 */
	while(1) {
		sin_size = sizeof(struct sockaddr_in);

		/* Accept actual connection from the client */
		if ((newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &sin_size)) == -1) {
			perror("ERRO on accepting socket");
			continue;	//give up and try listening next socket
		}

		printf("Receiving connection from %s\n", inet_ntoa(cli_addr.sin_addr));

		/* Create a thread to accept client */
		pthread_attr_init(&attr);
		pthread_create(&client_thread, &attr, response_socket, &newsockfd);

      /* Wait for the thread to exit */
		pthread_join(client_thread,NULL);
      printf("\nListening incoming call... (Press Ctrl + C to terminate at any time)\n");
	}

	pthread_exit(NULL);
   
   close(newsockfd); 
      
   return true;
}

void *response_socket(void *sid) {
   char response[256], buffer[256];
   int  n, socket_id;
   char* controlCode;
   int controlCodeValue;

   // cast the parameter to integer
   socket_id = *((int *)sid);

	/* If connection is established then start communicating */
	bzero(buffer,256);
	n = read(socket_id,buffer,255);
   
   if (n < 0) {
      perror("ERROR reading from socket");
      exit(1);
   }

   LinkedList* parameter = Parameter(buffer);

   // controller to determine which action to be taken
   controlCode = getParam(parameter, 0);
   controlCodeValue = atoi(controlCode);
   printf("Determining requested action...\n");
   if (controlCodeValue == REQUEST_VERIFY_LOGIN) {
      AuthRec* auth = Auth();
      strcpy(auth->username, getParam(parameter, 1));
      auth->pin = atoi(getParam(parameter, 2));

      if (verifyAuth(auth)) {
         printf("Login success.\n");
         ClientRec* clientRec = Client();
         printf("Client logged as: %d\n", auth->id);
         clientRec->id = auth->id;
         getAccountsForClient(clientRec);
         sprintf(response, "%d %s %s", auth->id, clientRec->firstName, clientRec->lastName);
         destroyClient(clientRec);
      } else {
         sprintf(response, "%d %s %s", 0, ".", ".");
         printf("Client login failed.\n");
      }
      destroyAuth(auth);
   } else if (controlCodeValue == REQUEST_GET_ACCOUNTS) {
      ClientRec* client = Client();
      client->id = atoi(getParam(parameter, 1));
      getAccountsForClient(client);

      AcRec* savingRec = Account(client->acSaving);
      if (client->acSaving > 0) {
         getAccountClosingBal(savingRec);
      }
      AcRec* loanRec = Account(client->acLoan);
      if (client->acLoan > 0) {
         getAccountClosingBal(loanRec);
      }
      AcRec* creditRec = Account(client->acCredit);
      if (client->acCredit > 0) {
         getAccountClosingBal(creditRec);
      }

      sprintf(response, "%d %d %d %.2f %.2f %.2f",
              client->acSaving, client->acLoan, client->acCredit,
              savingRec->closingBal, loanRec->closingBal, creditRec->closingBal);
      printf("response = %s\n", response);
      destroyAccount(savingRec);
      destroyAccount(loanRec);
      destroyAccount(creditRec);
      destroyClient(client);
   }
   destroyParameter(parameter);
   
   
   /* Write a response to the client */
   printf("sending response...\n");
   n = write(socket_id, response, 50);
   
   if (n < 0) {
      perror("ERROR writing to socket");
      exit(1);
   }

   printf("close response.\n");

   pthread_exit(0);
}
