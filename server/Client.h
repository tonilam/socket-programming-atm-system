#ifndef CLIENT_H_
#define CLIENT_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>

#include "Config.h"

typedef struct clientRec {
    int id;
    char firstName[MAX_NAME];
	char lastName[MAX_NAME];
    int acSaving;
    int acLoan;
    int acCredit;
} ClientRec;

/* Define constant for this class */


/* Define global variable for this class */
char datasetFile_Client[255];

/* Method signatures */
ClientRec* Client(void);
void destroyClient(ClientRec* instance);
bool getAccountsForClient(ClientRec* instance);
void testClient(void);

#endif