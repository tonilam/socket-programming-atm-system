#ifndef ACCOUNT_H_
#define ACCOUNT_H_


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "Config.h"

/**
 * Some notes for this class:
 * The id is the account number and is classified as follow:
 * Saving account = no. that is multiple of 11
 * Loan account = no. that is multiple of 12
 * Credit Card account = no. that is multiple of 13
 */

typedef struct acRec {
    int id;				// this should not be changed.
    double openingBal;  // this is the initial balance and the value never change.
    double closingBal;	// this is the only value can change.
} AcRec;

/* Define constant for this class */
#define SAVING_AC_ID 1
#define LOAN_AC_ID 2
#define CREDIT_AC_ID 3
#define INVALID_AC_ID -1

/* Define global variable for this class */
char* datasetFile_Ac;

/* Method signatures */
AcRec* Account(int id);
void destroyAccount(AcRec* instance);
int verifyAccountType(AcRec* instance);
bool getAccountClosingBal(AcRec* instance);
void testAccount(void);

#endif