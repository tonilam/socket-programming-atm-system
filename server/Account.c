#include "Account.h"

/**
 * This model is read-only.
 * This means no setter should be defined for this class.
 */

AcRec* Account(int id) {
    //This is the constructor of an authentication instance.
	AcRec* instance = malloc(sizeof(AcRec));
	datasetFile_Ac = (char*)malloc(256);
    sprintf(datasetFile_Ac, "%s%s", DATASET_PATH, FILE_ACCOUNT);

    instance->id = id;
    instance->openingBal = 0;
    instance->closingBal = 0;

    return instance;
}

void destroyAccount(AcRec* instance) {
	free(instance);
}

int verifyAccountType(AcRec* instance) {
	if (instance->id > 0) {
		if (DEBUG) printf("Checking Account No = %d\n", instance->id);

		if (instance->id % 11 == 0) {
			if (DEBUG) printf("This is a saving account.\n");
			return SAVING_AC_ID;
		} else if (instance->id % 12== 0) {
			if (DEBUG) printf("This is a load account.\n");
			return LOAN_AC_ID;
		} else if (instance->id % 13 == 0) {
			if (DEBUG) printf("This is a credit card account.\n");
			return CREDIT_AC_ID;
		} else {
			if (DEBUG) printf("This is an invalid account.\n");
			return INVALID_AC_ID;
		}
	}
	return INVALID_AC_ID;
}

bool getAccountClosingBal(AcRec* instance) {
	FILE *fp;
	int *acid = malloc(sizeof(int));
	double *openingBal = malloc(sizeof(double)),
		   *closingBal = malloc(sizeof(double));
	char garbage[255];
	bool match = false;

	// open a read only file stream
	fp = fopen(datasetFile_Ac, "r");

	// skip the first line of the datafile
	fscanf(fp, "%s%s%s", garbage, garbage, garbage);

	while (!feof(fp)) {
		// Checking each record from file
		fscanf(fp, "%d%lf%lf", acid, openingBal, closingBal);
		// matching the username and pin
		if (instance->id == *acid) {
			match = true;
			instance->openingBal = *openingBal;
			instance->closingBal = *closingBal;
		}
	}

	fclose(fp);
	free(acid);
	free(openingBal);
	free(closingBal);

	return match;
}

void testAccount(void) {
	AcRec* testingAccount = Account(12345674); // this no. is a multiple of 11
	testingAccount->closingBal = 100.0;
	testingAccount->openingBal = 30.0;
	verifyAccountType(testingAccount);
	printf("\nFollowing are the variable fro Account instance.\n");
	printf("dataset = %s\n", datasetFile_Ac);
	destroyAccount(testingAccount);
}
