/**
 *	This is the socket object to process communication with client.
 *
 *	Author: Toni Lam
 *	Date: October 2016
 */
#ifndef ATMSOCKET_H_
#define ATMSOCKET_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <unistd.h>
#include <pthread.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h> 

#include <string.h>

#include "Config.h"
#include "Account.h"
#include "Auth.h"
#include "Client.h"
#include "../library/RequestCode.h"
#include "../library/Parameter.h"

#define BACKLOG 10     /* how many pending connections queue will hold */

bool Socket(int portno);
void *response_socket(void *socket_id);

#endif