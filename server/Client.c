#include "Client.h"
#include "Account.h"

/**
 * This model is read-only.
 * This means no setter should be defined for this class.
 */

ClientRec* Client() {
    //This is the constructor of an authentication instance.
	ClientRec* instance = malloc(sizeof(ClientRec));

    sprintf(datasetFile_Client, "%s%s", DATASET_PATH, FILE_CLIENT);

    instance->firstName[0] = 0;
    instance->lastName[0] = 0;
    instance->acSaving = 0;
    instance->acLoan = 0;
    instance->acCredit = 0;

    return instance;
}

void destroyClient(ClientRec* instance) {
	free(instance);
}

bool getAccountsForClient(ClientRec* instance) {
	FILE *fp;
	char firstname[MAX_NAME];
	char lastname[MAX_NAME];
	unsigned int clientNo;
	char* accountlist= (char*)malloc(sizeof(char) * (MAX_ACCOUNTNO * 3 + 2)); // three accounts + 2 commas
	bool accountFound = false;

	// open a read only file stream
	fp = fopen(datasetFile_Client, "r");

	while (!feof(fp)) {
		// Checking each record from file
		fscanf(fp, "%s%s%u%s", firstname, lastname, &clientNo, accountlist);


		// find the target client's record
		if ((int)instance->id == clientNo) {
			accountFound = true;
			char* accounts;

			strcpy(instance->firstName, firstname);
			strcpy(instance->lastName, lastname);

			// split the accounts by commas and identify the account type
			accounts = strtok(accountlist, ",");
			while (accounts != NULL) {
				AcRec ac;
				ac.id = atoi(accounts);
				char type = verifyAccountType(&ac);
				if (type == SAVING_AC_ID) {
					instance->acSaving = ac.id;
				} else if (type == LOAN_AC_ID) {
					instance->acLoan = ac.id;
				} else if (type == CREDIT_AC_ID) {
					instance->acCredit = ac.id;
				}
				accounts = strtok(NULL, ",");
			}
		}


	}

	if (accountFound) {
		printf("Account Name: %s %s\n", instance->firstName, instance->lastName);
		printf("This client holds the following account:\n");
		if (instance->acSaving > 0) {
			printf("Saving account: %d\n", instance->acSaving);
		}
		if (instance->acLoan > 0) {
			printf("Loan account: %d\n", instance->acLoan);
		}
		if (instance->acCredit > 0) {
			printf("credit account: %d\n", instance->acCredit);
		}
		return true;
	} else {
		printf("Account not found!\n");
	}
	return false;
}

void testClient(void) {

	// check if the client holds 3 accounts
	ClientRec* client = Client();
	printf("\nFollowing are the variable from Client instance.\n");
	printf("dataset = %s\n", datasetFile_Client);
	client->id = 25418846;
	getAccountsForClient(client);
	destroyClient(client);

	// check if the client holds 1 account only
	ClientRec* client2 =	Client();
	client2->id = 35987154;
	getAccountsForClient(client2);
	destroyClient(client2);
}
