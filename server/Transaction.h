#ifndef TRANSACTION_H_
#define TRANSACTION_H_

#include "Config.h"

typedef struct transactionRec {
    int from;
    int to;
    int type;
    double amount;
} TransactionRec;

/* Define constant for this class */
#define DEPOSIT_TRANTYPE 2
#define WITHDRAWAL_TRANSTYPE 3
#define TRANSFER_TRANTYPE 4
#define INVALID_TRANTYPE -1

/* Define global variable for this class */
char* datasetFile_Transaction;

/* Method signatures */
void Transaction(TransactionRec* instance);
int verifyTransactionType(TransactionRec* instance);
int verifyTransaction(TransactionRec* instance);
void testTransaction(void);

#endif