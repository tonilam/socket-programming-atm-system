#include "Transaction.h"
#include <stdio.h>
#include <stdlib.h>

/**
 * This model is read-only.
 * This means no setter should be defined for this class.
 */

void Transaction(TransactionRec* instance) {
    //This is the constructor of an authentication instance.
	datasetFile_Transaction = (char*)malloc(256);
    sprintf(datasetFile_Transaction, "%s%s", DATASET_PATH, FILE_TRANSACTION);

}


int verifyTransactionType(TransactionRec* instance) {
	if (instance->type > 1) {
		if (DEBUG) printf("Checking Transaction Type = %d\n", instance->type);

		if (instance->type == DEPOSIT_TRANTYPE) {
			if (DEBUG) printf("This is a deposit transaction.\n");
			return DEPOSIT_TRANTYPE;
		} else if (instance->type == WITHDRAWAL_TRANSTYPE) {
			if (DEBUG) printf("This is a withdrawal transaction.\n");
			return WITHDRAWAL_TRANSTYPE;
		} else if (instance->type == TRANSFER_TRANTYPE) {
			if (DEBUG) printf("This is a transfer transaction.\n");
			return TRANSFER_TRANTYPE;
		} else {
			if (DEBUG) printf("This is an invalid transaction.\n");
			return INVALID_TRANTYPE;
		}
	}
	return INVALID_TRANTYPE;
}

int verifyTransaction(TransactionRec* instance) {
	int tranType = verifyTransactionType(instance);
	if ((tranType >= 2) && (tranType <= 4)) {
		if (DEBUG) printf("Transaction type verification [PASSED].\n");
		if ((tranType == 2) || (tranType == 3)) {
			if (instance->to == instance->from) {
				if (DEBUG) printf("Accounts (to & from) verification [PASSED].\n");
				return 1;
			} else {
				if (DEBUG) printf("Accounts (to & from) verification [FAILED].\n");
				return -1;
			}
		} else { // transType == 4
			if (instance->to == instance->from) {
				if (DEBUG) printf("Accounts (to & from) verification [FAILED].\n");
				return -1;
			} else {
				if (DEBUG) printf("Accounts (to & from) verification [PASSED].\n");
				return 1;
			}
		}
	} else {
		if (DEBUG) printf("Transaction type verification [FAILED].\n");
		return -1;
	}
	return -1;
}

void testTransaction(void) {
	TransactionRec testingTransaction;
	testingTransaction.type = 4;
	testingTransaction.from = 12345678;
	testingTransaction.to = 87654321;
	verifyTransaction(&testingTransaction);
	printf("\nFollowing are the variable fro Transaction instance.\n");
	printf("dataset = %s\n", datasetFile_Transaction);
}
