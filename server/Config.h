/* system configuration */
#ifndef CONFIG_H_
#define CONFIG_H_

#include <stdbool.h>
#include <limits.h>

#define DATASET_PATH "./data/"
#define FILE_AUTH "Authentication.txt"
#define FILE_ACCOUNT "Accounts.txt"
#define FILE_CLIENT "Client_Details.txt"
#define FILE_TRANSACTION "Transactions.txt"

#define DEBUG false

#define DEFAULT_PORT 12345

#define MAX_USERNAME 30
#define MAX_PIN 6
#define MAX_CLIENTNO 8
#define MAX_NAME 30
#define MAX_ACCOUNTNO 8

#define CREDIT_LIMIT 5000
#define MIN_SAVING 0

#endif