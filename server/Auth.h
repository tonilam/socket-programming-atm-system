#ifndef AUTH_H_
#define AUTH_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>

#include "Config.h"

typedef struct authRec {
    int id;
    char username[MAX_USERNAME];
    int pin;
} AuthRec;

/* Define constant for this class */


/* Define global variable for this class */
char* datasetFile_Auth;

/* Method signatures */
AuthRec* Auth(void);
void destroyAuth(AuthRec* instance);
bool verifyAuth(AuthRec* instance);
void testAuth(void);

#endif