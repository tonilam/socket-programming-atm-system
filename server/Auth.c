#include "Auth.h"

/**
 * This model is read-only.
 * This means no setter should be defined for this class.
 */

AuthRec* Auth() {
    //This is the constructor of an authentication instance.
	AuthRec* instance = malloc(sizeof(AuthRec));
	datasetFile_Auth = (char*)malloc(256);
    sprintf(datasetFile_Auth, "%s%s", DATASET_PATH, FILE_AUTH);

    instance->id = 0;
    instance->username[0] = 0;
    instance->pin = 0;

    return instance;
}

void destroyAuth(AuthRec* instance) {
	free(instance);
}

bool verifyAuth(AuthRec* instance) {
	FILE *fp;
	char* username = (char*)malloc(sizeof(char) * MAX_USERNAME);
	int* pin = (int*)malloc(sizeof(char) * MAX_PIN);
	int* clientNo = (int*)malloc(sizeof(char) * MAX_CLIENTNO);;
	bool match = false;

	// open a read only file stream
	fp = fopen(datasetFile_Auth, "r");

	while (!feof(fp)) {
		// Checking each record from file
		fscanf(fp, "%s%d%d", username, pin, clientNo);

		// matching the username and pin
		if ( (strcmp(instance->username, username) == 0)
			 && ((int)instance->pin == (int)*pin)
			) {
			match = true;
			instance->id = *clientNo;
		}
	}

	fclose(fp);
	free(username);
	free(pin);
	free(clientNo);

	return match;
}

void testAuth(void) {
	printf("\nFollowing are the variable fro Auth instance.\n");
	printf("dataset = %s\n", datasetFile_Auth);

	AuthRec* auth = Auth();

	// test correct login
	strcpy(auth->username, "elasticity");
	auth->pin = 1664;
	if (verifyAuth(auth)){
		printf("Login success, client no is: %d\n", auth->id);
	} else {
		printf("Login failed\n");
	}
	destroyAuth(auth);

	// test incorrect login
	AuthRec* auth2 = Auth();
	strcpy(auth2->username, "tonilam");
	auth2->pin = 8888;
	if (verifyAuth(auth2)){
		printf("Login success, client no is: %d\n", auth2->id);
	} else {
		printf("Login failed\n");
	}
	destroyAuth(auth2);
}
